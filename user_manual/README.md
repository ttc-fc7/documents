###### Introduction

User manual for TTC-uDTC firmware.

###### 1. Edit text in the corresponding .tex documents in /tex directory

```
TTC-uDTC-firmware-manual_overleaf.tex --> Overleaf editable TDR document
TTC-uDTC-firmware-manual.tex          --> main TDR document to make with tdr script
titlepage.tex                         --> title page
chapter_1.tex                         --> 1st chapter
...                                   
chapter_5.tex                         --> 2nd chapter
bib.tex                               --> bibliography
```

###### 2. Download Overleaf project

```
$ mkdir user_manual
$ cd user_manual
$ git clone https://git.overleaf.com/623f092a0cd6af9e1001af10
$ cd 623f092a0cd6af9e1001af10
```

###### 3. Compile the document(s)

```
$ make draft
$ make nodraft || make all
```
