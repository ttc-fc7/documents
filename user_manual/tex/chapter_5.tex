\definecolor{gray}{rgb}{0.33, 0.33, 0.33}
%-------------------------------------------------------------------
\chapter{Performing routine test procedures}
%-------------------------------------------------------------------

Once the test setup is built and all software is installed, routine tests can begin. This section provides a detailed description of testing the simplified TTS state machine without OOS state on a TTC-AMC13-OT chain. The test consists of two parts, first we do not send triggers and then we send triggers. The latter case is not described because it is straightforward after taking the first one. This test is sufficent to lay the groundwork for further improvements. Finally, in the last subsection, there is a troubleshooting where solutions to specific problems can be found based on practical experiences.

\begin{left} \textbf{Questions so far in troubleshooting:} \end{left} \linebreak
1. What can I do if the FC7 is not reacting after bitstream upload? \\
2. How to install AMC13Tool2 on CentOS 8? \\
3. How to switch to external clock source? \\
4. How to perform loopback testing with AMC13? \\
5. How to perform a simple trigger test? \\
6. How should be the connection xml file for AMC13, pyOT and pyTTC configured? \\
7. What is the digital channel assignment of the DIO5 after configuration?

\clearpage

\begin{footnotesize}

%-------------------------------------------------------------------
\section{Testing TTS-FSM in TTC-AMC13-OT chain}
%-------------------------------------------------------------------

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.75\textwidth}{tex/figures/ITOT_TTS_FSM.png}{tex/figures/ITOT_TTS_FSM.png}
      \caption[Caption for TOC]{TTS state machine in IT- or OT-µDTC.\label{fig:test}}
}
\end{figure}

\underline{Note:} \linebreak
  \parbox{\textwidth}{															\\
	→ OOS TTS state was removed from the TTS FSM of OT-µDTC upon request							\\
	→ still reacting to TTC resync sent to other cards in the crate by re-synchronizing internal state with other cards	\\
	→ tested TTS FSM by manually initiating possible state transitions							\\
	→ \textcolor{red} {actual FE read-out, and checking of buffer levels did not take place}
  }%

\end{footnotesize}
\begin{normal}
a) without trigger
\end{normal}
\begin{footnotesize}

\begin{table}[h]
\footnotesize
  \begin{left}
    \label{tab:mytab}
    \begin{tabular}{ll}
                                                                                        & TTS state transition     \\ \hline
       1) synchronize                                                                   & 04 → 08                  \\
       2) ipbus\_any\_buffer\_full (0) → 1 →  0  ipbus\_all\_buffer\_empty (0) → 1 → 0  & 08 → 14 → 08             \\
       3) ipbus\_any\_buffer\_full (0) → 1 →  0                                         & 08 → 14                  \\
       4) resync command                                                                & 14 → 24 → 34 → 14        \\
       5) ipbus\_all\_buffer\_empty (0) → 1                                             & 14 → 08                  \\
       6) resync command                                                                & 08 → 24 → 34 → 14 → 08   \\ \hline
    \end{tabular}
  \end{left}
\end{table}

Result: All TTS state transitions take place!

\end{footnotesize}
\begin{normal}
b) with triggers
\end{normal}
\begin{footnotesize}
  
\begin{table}[h]
\footnotesize
  \begin{left}
    \label{tab:mytab}
    \begin{tabular}{ll}
                                                                                        & TTS state transition     \\ \hline
       1) synchronize                                                                   & 04 → 08                  \\
       trigger                                                                          &                          \\
       2) ipbus\_any\_buffer\_full (0) → 1 →  0  ipbus\_all\_buffer\_empty (0) → 1 → 0  & 08 → 14 → 08             \\
       trigger                                                                          &                          \\
       3) ipbus\_any\_buffer\_full (0) → 1 →  0                                         & 08 → 14                  \\
       trigger                                                                          &                          \\
       4) resync command                                                                & 14 → 24 → 34 → 14        \\
       trigger                                                                          &                          \\
       5) ipbus\_all\_buffer\_empty (0) → 1                                             & 14 → 08                  \\
       trigger                                                                          &                          \\
       6) resync command                                                                & 08 → 24 → 34 → 14 → 08   \\
       trigger                                                                          &                          \\ \hline
    \end{tabular}
  \end{left}
\end{table}

Result: All TTS state transitions took place and triggers logged in TTC-FC7!

\underline{Note:} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
(0) → 1 → 0       & - from (0) initial value, control signal is switched to 1, then back to 0 \\
                  & - for example, buffers cannot be full and empty at the same time, so we have to go back to zero after one of the control signals was 1
\end{tabular}

\end{footnotesize}

\clearpage

c) detailed description of testing procedure

\begin{scriptsize}

0) initialization process

- upload bitsream to TTC-µDTC \\
- upload bitsream to OT-µDTC

- reset Spartan 6 and Kintex 7 in AMC13 with AMC13Tool2 \\
$>$ws 0x0 0x10 → \textcolor{blue}{source env.sh first !}\\
$>$wv 0x0 0x0

- configure TTC-µDTC \\
  \$ ttc configure → \textcolor{blue}{source env.sh first !} \\
→ switch L8, L12 and GBT power supplies on \\
→ enable 8SFP FMC i2c master \\
→ program 8SFP FMC i2c master to enable TX transciever to send out TTC clock and trigger to AMC13 \\
→ enable DIO5 i2c master \\
→ turn on DIO5 load config to configure DIO5 \\
→ switch DIO5 channel 1,3 and 4 output enables on  \\
→ turn off DIO5 load config when configuration is done

  \$ ttc log dump \\
→ check if TTS logging is buggy in transition logger, in case of error or infinite loop all previous steps should be repeated from the beginning until it works properly 

- configure OT-µDTC \\
  \$ ot configure → \textcolor{blue}{source env.sh first !}\\
\# enable trigger accept \\
→ reset TTC decoder \\
→ enable TTC decoder to lock on AMC13 clock later on \\
→ reset fast command processor \\
→ select Level 1 trigger source in fast command processor \\
→ set triggers to accept in fast command processor to 0, in order to accept unlimited number of triggers \\
→ disable backpressure\_enable in fast command processor \\
→ load configuration in fast command processor \\
→ start triggering in fast command processor \\
→ check the status of fast command processor \\
\# enable TTS FSM \\
→ turn on TTS FSM \\
\# switch to AMC13 clock source \\
→ switch to AMC13 clock source sent by AMC13 through the backplane \\
→ general reset \\
→ check status of clock generators

- configure AMC13 \\
\begin{tabular}{p{4cm} p{12.5cm}}
$>$wv 0x8 0x00000004          & → set BX offset to 4  in order to avoid BcN mismatches between OT and AMC13 \\
$>$wv 0x0000001c 0x30000000   & → turn off trigger rules, TTC defines trigger rules \\
$>$wv 0x3 0x80000000          & → ignore DAQ data, no data read out from AMC13 \\
$>$en 7                       & → enable AMC13 clock to OT-µDTC, here it is in the 7th AMC slot \\
$>$st                         & → BUSY(x”04”) → READY(x”08”) TTS state transition took place
\end{tabular}

TTS FSM state transition testing

1) synhronization process

- starting orbit structure \\
→ sending BC0 periodically \\
  \$ ttc bc0 periodic\_bc0\_on \\
→ sending EC0 to zero event counters \\
  \$ ttc bgo ec0 \\
→ sending OC0 to synchronize orbits in TTC-AMC13-OT chain \\
  \$ ttc bgo oc0

TTS state machines \\
→ TTS FSM enabled in TTC-µDTC by default \\
→ TTS FSM enabled in OT-µDTC by default

Backpressure is disabled in the fast command processor by default. When backpressure is disabled, BUSY and READY TTS state transitions can be controlled with ipbus registers according to the block diagram, otherwise the data processing block (be\_proc) takes charge of buffer states. For triggers to veto, it is crucial to know if any of the buffers are almost full and whether all buffers are empty. Care must be taken to ensure that the two conditions cannot be met at the same time!

\clearpage

2) READY(x”08”) → BUSY\_1(x”14”) → READY(X”08”)

- switching any\_buffer\_full \& all\_buffer\_empty to 1 from 0 and back to 0 \\
  \$ ot tts set ipbus\_any\_buffer\_full 1 \\
→ READY → BUSY\_1 state transition takes place \\
  \$ ot tts set ipbus\_any\_buffer\_full 0 \\
→ turn it off, when next time we come along do not change state immediately \\
  \$ ot tts set ipbus\_all\_buffer\_empty 1 \\
→ BUSY\_1 → READY state transition takes place \\
  \$ ot tts set ipbus\_all\_buffer\_empty 0 \\
→ turn it off, when next time we come along do not change state immediately

3) READY(x”08”) → BUSY\_1(x”14”)

- switching ipbus\_any\_buffer\_full from 0 to 1 and back to 0 \\
  \$ ot tts set ipbus\_any\_buffer\_full 1 \\
→ READY → BUSY\_1 state transition takes place \\
  \$ ot tts set ipbus\_any\_buffer\_full 0 \\
→ turn it off, when next time we come along do not change state immediately

4) BUSY\_1(x”14”) → BUSY\_2(x”24”) → BUSY\_3(x”34”) → BUSY\_1(x”14”)

- sending resync sequence \\
  \$ ttc bgo resync\_seq \\
→ after resync sequence BUSY\_1 state transition takes place, because all three conditions were met

5) BUSY\_1(x”14”) → READY(x”08”)

- switching ipbus\_all\_buffer\_empty from 0 to 1 \\
  \$ ot tts set ipbus\_all\_buffer\_empty 1 \\
→ BUSY\_1 → READY state transition takes place

6) READY(x”08”) → BUSY\_2(x”24”) → BUSY\_3(x”34”) → BUSY\_1(x”14”) → READY(x”08”)

- sending resync sequence \\
  \$ ttc bgo resync \\
→ after resync sequence READY state transition takes place, because all four conditions were met

7) checking results

-  dumping transition logger in TTC-µDTC \\
  \$ ttc log dump \\
\begin{tiny}
\begin{tabular}{p{5cm} p{6cm}}
Number of state transition log entries  36      & \\
000000 :  [TTS], BCN= 1697 , TTS= RDY(8)        & \\
000001 :  [BC0], ORB= 69377 , BC0\_en= 0        & \\
000002 :  [TTS], BCN= 3177 , TTS= BSY(4)        & \\
000003 :  [BC0], ORB= 69411 , BC0\_en= 0        & \\
000004 :  [TTS], BCN= 3033 , TTS= RDY(8)        & \\
000005 :  [BC0], ORB= 69470 , BC0\_en= 0        & \\
000006 :  [BGO], BCN= 3114 , BGO= EC0           & \\
000007 :  [BC0], ORB= 24236 , BC0\_en= 1	& → 1) synchronize \\
000008 :  [BGO], BCN= 1432 , BGO= OC0           & \\
000009 :  [CHB], BCN= 1440 , ChB Cmd= EC0       & \\
000010 :  [BC0], ORB= 0 , BC0\_en= 1            & \\
000011 :  [TTS], BCN= 1077 , TTS= BSY(4)	& → 2) ipbus\_any\_buffer\_full 1 \\
000012 :  [BC0], ORB= 33754 , BC0\_en= 1	& \; \; \; \; \ \ ipbus\_any\_buffer\_full 0 \\
000013 :  [TTS], BCN= 449 , TTS= RDY(8)	        & \; \; \; \; \ \ ipbus\_all\_buffer\_empty 1 \\
000014 :  [BC0], ORB= 228287 , BC0\_en= 1       & \; \; \; \; \ \ ipbus\_all\_buffer\_empty 0 \\
000015 :  [TTS], BCN= 2361 , TTS= BSY(4)	& → 3) ipbus\_any\_buffer\_full 1 \\
000016 :  [BC0], ORB= 215039 , BC0\_en= 1       & \; \; \; \; \ \ ipbus\_any\_buffer\_full 0 \\
000017 :  [BGO], BCN= 1366 , BGO= TCDS 4/Resync & → 4) RESYNC sequence \\
000018 :  [CHB], BCN= 1374 , ChB Cmd= OC0       & \\
000019 :  [CHB], BCN= 2023 , ChB Cmd= Resync    & \\
000021 :  [BC0], ORB= 243062 , BC0\_en= 1       & \\
000022 :  [CHB], BCN= 2023 , ChB Cmd= EC0       & \\
000023 :  [BC0], ORB= 243063 , BC0\_en= 1       & \\
000024 :  [TTS], BCN= 3261 , TTS= RDY(8)        & → 5) ipbus\_all\_buffer\_empty 1 \\
000025 :  [BC0], ORB= 110611 , BC0\_en= 1       & \\
000026 :  [BGO], BCN= 1985 , BGO= TCDS 4/Resync & → 6) RESYNC sequence \\
000028 :  [CHB], BCN= 2023 , ChB Cmd= Resync    & \\
000029 :  [TTS], BCN= 2037 , TTS= BSY(4)        & \\
000031 :  [BC0], ORB= 252741 , BC0\_en= 1       & \\
000032 :  [CHB], BCN= 2023 , ChB Cmd= EC0       & \\
000033 :  [BC0], ORB= 252742 , BC0\_en= 1       & \\
000034 :  [TTS], BCN= 1893 , TTS= RDY(8)        & \\
000035 :  [BC0], ORB= 252800 , BC0\_en= 1       & \\
000034 :  [TTS], BCN= 1895 , TTS= RDY(8)
\end{tabular}
\end{tiny}

\end{scriptsize}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\clearpage

%-------------------------------------------------------------------
\section{Troubleshooting}
%-------------------------------------------------------------------

\begin{small}
%-------------------------------------------------------------------

\vspace{2.5mm}

\begin{left} \textbf{1. What can I do if the FC7 is not reacting after bitstream upload?} \end{left}

Rarely, but sometimes, it happens that the microcontroller of the FC7, which is responsible for uploading the bitstream, is stuck and we have to restart the FC7 in the µTCA crate remotely. The following procedure requires telnet package to be installed.

\$ telnet 192.168.4.41 → \textcolor{blue}{MCH IP address} \\
$>$show\_fru                 \\
$>$hidden                    \\
$>$hard\_reset $<$fru\_id$>$

\begin{left} \textbf{2. How to install AMC13Tool2 on CentOS 8?} \end{left}

→ checking package versions

\$ sudo yum install ipmitool readline-devel boost-devel gcc \\
\$ sudo nano /etc/yum.repos.d/ipbus-sw.repo

\begin{scriptsize}
$[$ipbus-sw-base$]$ \\
name=IPbus software repository \\
baseurl=http://www.cern.ch/ipbus/sw/release/2.8/repos/centos8/x86\_64/base/RPMS \\
enabled=1 \\
gpgcheck=0 \\ \\
$[$ipbus-sw-updates$]$ \\
name=IPbus software repository updates \\
baseurl=http://www.cern.ch/ipbus/sw/release/2.8/repos/centos8/x86\_64/updates/RPMS \\
enabled=1 \\
gpgcheck=0
\end{scriptsize}

\$ sudo yum clean all \\
\$ sudo yum groupinstall uhal controlhub

→ downloading new uhal 2.8 branch from amc13 repository

\$ wget https://gitlab.cern.ch/cms-cactus/boards/amc13/-/archive/uhal-2.8/amc13-uhal-2.8.tar.gz \\
\$ tar -xvzf amc13-uhal-2.8.tar.gz 

→ install amc13

\$ cd amc13-uhal-2.8/python/                    \\
\$ git clone https://github.com/pybind/pybind11 \\
\$ cd pybind11/                                 \\
\$ git checkout v2.8                            \\
\$ cd ../..                                     \\
\$ source env.sh                                \\
\$ make

→ create connections.xml with TCP connection

\$ cd amc13/etc/amc13                           \\
\$ cp connectionSN43.xml connections.xml        \\
\$ nano connections.xml                         \\
\begin{scriptsize}                              \\
$<$?xml version="1.0" encoding="UTF-8"?$>$

$<$connections$>$ \\
$<$connection id="T1" uri="chtcp-2.0://localhost:10203?target=192.168.4.165:50001" address\_table="file://AMC13XG\_T1.xml" /$>$ \\
$<$connection id="T2" uri="chtcp-2.0://localhost:10203?target=192.168.4.164:50001" address\_table="file://AMC13XG\_T2.xml" /$>$ \\
$<$/connections$>$ 
\end{scriptsize}

\clearpage

→ run AMC13Tool2

\$ cd ../../../tools/bin/                                                   \\
\$ source ../../env.sh                                                      \\
\$ AMC13Tool2.exe -c ../../amc13/etc/amc13/connections.xml                  \\
\textcolor{gray}{
No address table path specified.                          		    \\
Using .xml connection file...                                               \\
Using AMC13 software ver:0                                                  \\
Read firmware versions 0x2264 0x32                                          \\
flavor = 2  features = 0x000000b2					    \\
}					                                          				     
$>$ws 0x0 0x10                                                              \\
\textcolor{gray}{
Write to address 0X0                                                        \\
}
$>$wv 0x0 0x0                                                               \\
\textcolor{gray}{
Write to address 0X0                                                        \\
}
$>$ ...                                                                    

\begin{left} \textbf{3. How to switch to external clock source?} \end{left}

Connect a 40,08 MHz TTL clock to DIO5 channel 5. When clock source is switched to the external clock with the XPOINT switch and the BUFGMUX, MMCM and PLL should lock and clock rate tool should meaasure the clock frequencies properly. For example if you configure an LVTTL 40.08MHz square wave with 3.3V voltage high and 0 voltage low, you must enable the 50 Ohm terminator on channel 5 and set the threshold value to half of the voltage high, in this case to 1.65 Volts.

\$ cd pyTTC/ \\
\$ source env.sh \\
\$ ttc fmc on → \textcolor{blue}{power on FMCs}\\
\$ ttc dio5 set term 5 1 → \textcolor{blue}{enable 50 Ohm terminator on 5th channel}\\
\$ ttc dio5 set thr\_V 5 1.65 → \textcolor{blue}{set 5th channel threshold to 1.65 Volts}\\
\$ ttc cnc clk\_rate set clk 1  → \textcolor{blue}{0: on-board clock source, 1: external clock}\\
\textcolor{gray}{
Measuring clocks in the firmware ... \\
------------------------------------------------    \\
clk\_rate\_1 --$>$ ipb\_clk                    31250000  \\
clk\_rate\_2 --$>$ fabric\_clk                 40078429  \\
clk\_rate\_3 --$>$ clk\_40MHz                  40078429  \\
clk\_rate\_4 --$>$ clk\_80MHz                  80156858  \\
clk\_rate\_5 --$>$ clk\_160MHz                 160313716 \\
clk\_rate\_6 --$>$ osc125\_a\_mgtrefclk\_i       125000000 \\
clk\_rate\_7 --$>$ clk\_400MHz                 400784289 \\
clk\_rate\_8 --$>$ clk\_200MHz                 125000000 \\
clk\_rate\_9 --$>$ ref\_clk\_200MHz             400784289 \\
------------------------------------------------
}

\$ ttc cnc clk\_rate \\
\textcolor{gray}{
Measuring clocks in the firmware ... \\
------------------------------------------------ \\
clk\_rate\_1 --$>$ ipb\_clk                    31250000 \\
clk\_rate\_2 --$>$ fabric\_clk                 40078431 \\
clk\_rate\_3 --$>$ clk\_40MHz                  40078431 \\
clk\_rate\_4 --$>$ clk\_80MHz                  80156863 \\
clk\_rate\_5 --$>$ clk\_160MHz                 160313726 \\
clk\_rate\_6 --$>$ osc125\_a\_mgtrefclk\_i       125000000 \\
clk\_rate\_7 --$>$ clk\_400MHz                 400784314 \\
clk\_rate\_8 --$>$ clk\_200MHz                 125000000 \\
clk\_rate\_9 --$>$ ref\_clk\_200MHz             400784314 \\
------------------------------------------------
}

\clearpage

\begin{left} \textbf{4. How to perform loopback testing with AMC13?} \end{left}

AMC13 in loopback mode generates orbit signal with a bunch structure, sends 0xe600ffff hex periodically. Here, 0xe6 is an EC0 and 0xFFFF is a combination of EC0 and BC0 both. It is possible to check in debug core exactly what OT-FC7 is decoding with TTC decoder when AMC13 is in loopback mode.

→ upload bitstream with Ph2\_ACF to OT-FC7

→ reset firmwares in AMC13 with AMC13Tool2

$>$ws 0x0 0x10 \\
$>$wv 0x0 0x0

→ configure OT-FC7 with pyOT

\$ ot configure → \textcolor{blue}{source env.sh first !}

→ configure AMC13

\begin{tabular}{p{5cm} p{12.5cm}}
$>$wv 0x8 0x00000004         & → BX offset \\
$>$wv 0x0000001c 0x30000000  & → Trigger rule \\
$>$wv 0x3 0x80000000         & → ignore DAQ data (TTS still working) \\
$>$en 1-12 f t               & → enable OT clock \\
$>$st                        & → TTS\_state → READY
\end{tabular}

→ send triggers with AMC13

$>$lt c  → \textcolor{blue}{enable continous trigger mode} \\
$>$st → \textcolor{blue}{status}

\begin{left} \textbf{5. How to perform a simple trigger test?} \end{left}

→ upload bitstreams to OT- and TTC-FC7s

$[$1$]$\$ fpgaconfig -i OT.bit -c OT\_FC7.xml \\
$[$1$]$\$ fpgaconfig -i TTC.bit -c TTC\_FC7.xml

→ reset firmwares in AMC13 with AMC13Tool2

$[$2$]$\$ cd amc13-1.2.9/tools/bin/ \\
$[$2$]$\$ source ../../env.sh \\
$[$2$]$\$ AMC13Tool2.exe -c ../../amc13/etc/amc13/connections.xml \\
$>$ws 0x0 0x10 \\
$>$wv 0x0 0x0

→ configure TTC-FC7 with pyTTC

$[$3$]$\$ cd path/to/pyTTC/ \\
$[$3$]$\$ source env.sh \\
$[$3$]$\$ ttc configure \\
$[$3$]$\$ ttc log dump → \textcolor{blue}{if there is infinite loop, start from scratch until it disappears !}} \\
$[$3$]$\$ ttc fmc set\_as\_trigger\_source → \textcolor{blue}{spare pin trigger loopback gives better timing performance}}

→ configure OT-FC7 with pyOT

$[$4$]$\$ cd path/to/pyOT/ \\
$[$4$]$\$ source env.sh \\
$[$4$]$\$ ot configure

→ configure AMC13

\begin{tabular}{p{5cm} p{12.5cm}}
$>$wv 0x8 0x00000004         & → BX offset \\
$>$wv 0x0000001c 0x30000000  & → Trigger rule \\
$>$wv 0x3 0x80000000         & → ignore DAQ data (TTS still working) \\
$>$en 7                      & → enable OT clock
\end{tabular}

→ check TTS state in AMC13 and TTC-FC7

$>$st → \textcolor{blue}{TTS state → READY !}} \\
$[$3$]$\$ ttc tts state → \textcolor{blue}{TTS state → READY !}}

→ starting orbit structure

$[$3$]$\$ ttc bc0 periodic\_bc0\_on \\
$[$3$]$\$ ttc bgo ec0 \\
$[$3$]$\$ ttc bgo oc0

→ sending 1 trigger

$[$3$]$\$ ttc trg ipbus send 1

→ check L1A counters

$[$3$]$\$ ttc cnc cnt \\
$>$st \\
$[$4$]$\$ ot cnc cnt

Trigger should be seen in TTC, AMC13 and OT statuses!

\begin{left} \textbf{6. How should be the connection xml file for AMC13, pyOT and pyTTC configured?} \end{left}

The following example only applies to AMC13, but pyOT and pyTTC can be configured similarly.

\$ cd amc13-1.2.9/amc13/etc/amc13/ \\
\$ cp connectionSN43.xml connections.xml \\
\$ nano connections.xml

→ connections.xml with UDP connection ($<$ CentOS 7.6)

\begin{tiny} 
$<$?xml version="1.0" encoding="UTF-8"?$>$

$<$connections$>$ \\
$<$connection id="T1" uri="ipbusudp-2.0://192.168.4.199:50001" address\_table="file://AMC13XG\_T1.xml" /$>$ \\
$<$connection id="T2" uri="ipbusudp-2.0://192.168.4.198:50001" address\_table="file://AMC13XG\_T2.xml" /$>$ \\
$<$/connections$>$
\end{tiny} 

→ connections.xml with TCP connection to localhost

\begin{tiny} 
$<$?xml version="1.0" encoding="UTF-8"?$>$

$<$connections$>$ \\
$<$connection id="T1" uri="chtcp-2.0://localhost:10203?target=192.168.4.183:50001" address\_table="file://AMC13XG\_T1.xml" /$>$ \\
$<$connection id="T2" uri="chtcp-2.0://localhost:10203?target=192.168.4.182:50001" address\_table="file://AMC13XG\_T2.xml" /$>$ \\
$<$/connections$>$ 
\end{tiny}

→ connections.xml with TCP connection to ph2acf-02.cern.ch domain name

\begin{tiny} 
$<$?xml version="1.0" encoding="UTF-8"?$>$

$<$connections$>$ \\
$<$connection id="T1" uri="chtcp-2.0://ph2acf-02.cern.ch:10203?target=192.168.4.165:50001" address\_table="file://AMC13XG\_T1.xml" /$>$ \\
$<$connection id="T2" uri="chtcp-2.0://ph2acf-02.cern.ch:10203?target=192.168.4.164:50001" address\_table="file://AMC13XG\_T2.xml" /$>$ \\
$<$/connections$>$
\end{tiny}

\begin{left} \textbf{7. What is the digital channel assignment of the DIO5 after configuration?} \end{left}

\begin{table}[h]
\footnotesize
  \begin{center}
    \label{tab:mytab}
    \begin{tabular}{lcl}
       channel 1: & OUT & All triggers (generated and external feed-back) \\
       channel 2: & IN  & External trigger                                \\
       channel 3: & OUT & 40 MHz User-core clock (clk\_40MHz)                          \\
       channel 4: & OUT & External trigger (inverted feed-back of Ch 2)   \\
       channel 5: & IN  & External clock                                  \\
    \end{tabular}
  \end{center}
\end{table}

%-------------------------------------------------------------------
\end{small}
