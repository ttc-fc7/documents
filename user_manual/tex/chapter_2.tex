%-------------------------------------------------------------------
\chapter{TTC µDTC firmware architecture}
%-------------------------------------------------------------------

The Timing, Trigger and Control (TTC) µDTC firmware provides a straightforward framework for sending clock, trigger and BGo commands in µTCA crates to other FC7 based subsystems like IT/OT-µDTC through the AMC13. Synchronization is established in this readout chain by enabling the clock distribution in each AMC slot. TTC-µDTC is also capable of reading back the actual TTS state from AMC13, which can be changed by either AMC13 or one of the FC7s in the µTCA crate. After certain TTS state transitions it is necessary to intervene with the TTC-µDTC to restore synchronization by sending BGo commands. During the acquisition of data, the transition logger also plays an essential role in the subsequent data analysis and debugging, which logs details of particular events that can be read using the TTC software called TTC-FC7-Control. TTC-µDTC-firmware for the hardware and TTC-FC7-Control software for the data readout are still in development phase and this documentation only contains the firmware architecture in a nutshell with basic instructions on how to assemble the system, set up a µTCA crate and perform routine tests on it. For a more detailed understanding checking Trigger and Clock Distribution System (TCDS) site on CERN Twiki Sandbox \cite{bib:TCDS_Twiki} and TTC-µDTC-firmware repository \cite{bib:TTC-uDTC-firmware} are also recommended. TCDS Twiki site is very detailed, contains too much information and seems outdated. However, due to the longer EOL in case of CentOS 7, it is not out of date, as updates will be available for a couple of years. 

TTC-µDTC-firmware is essentially builds up of system and user cores. For the sake of simplicity, we deal with the user core in detail here, because the system core has not changed much after the production tests, only the update of the ipbus firmware became necessary as new UltraScale+ FPGAs appeared. Basically, the cores comprises of the components, blocks or modules that can be seen on Figure 1. on next page.

The user core of the firmware contains the following blocks:

\begin{itemize}
\item \texttt{Physical interface abstraction layer} buffering and routing from/to FMCs
\item \texttt{DIO5 I2C $\&$ TLU} external clock and trigger input, delay and measure trigger phase
\item \texttt{8SFP I2C} duplex optical communication with AMC13, TTC output and TTS input
\item \texttt{Clock generator} to generate clocks for the user core and measure them
\item \texttt{TTC L1A trigger} to generate filter and bookkeep L1A trigger phase
\item \texttt{TTC BGo commands} for sending BGo commands to other FC7s in the µTCA crate
\item \texttt{Counters} to log EvN, EvrN, BcN and OrN numbers for data analysis
\item \texttt{TTS logic} for decoding TTS state from AMC13 and control with FSM
\item \texttt{Transition logger} to log trigger, BGo and counter values in orbit structure
\end{itemize}

Some blocks can be further subdivided, which are presented in the following sections. 

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.95\textwidth}{tex/figures/TTC-uDTC-firmware.png}{tex/figures/TTC-uDTC-firmware.png}
      \caption[Caption for TOC]{TTC-µDTC-firmware block diagram.\label{fig:test}}
}
\end{figure}

%-------------------------------------------------------------------
\section{Physical interface abstraction layer}
%-------------------------------------------------------------------

This is a very useful part of the core inherited from the Phase-2 d19c-firmware. The benefits of this block can be really exploited when the implementation of a wide variety of frontends become necessary in the firmware. For TTC-µDTC-firmware, this is not really the case, because the location of DIO5 and 8SFP FMC cards are fixed and only a small number of buffers are required. This block also includes an fmc io mapping, which requires the mapping of the FMC IO pins of the FMC cards. The /doc folder contains pin assignments of the FMC cards \cite{bib:pinaDIO5} \cite{bib:pina8SFP}. Knowing these is essential, for example, connecting two pins with different potentials is not recommended. In summary, physical interface abstraction layer is responsible for the buffering of input/output/inout ports of the L8 and L12 FMC slots. In the current state of development, DIO5 is implemented on L8 and 8SFP is implemented on L12 FMC slots. 

%-------------------------------------------------------------------
\section{I2C masters for FMC cards}
%-------------------------------------------------------------------

According to the current state of development there are three i2c masters in the firmware, one of them is inside the system core, where it performs system-wide operations. Two i2c masters implemented in the user core, the first for programming the DIO5 and the second for programming the 8SFP FMC card. These two i2c masters are hard wired from the FMC connectors and routed to the same trisate io pin of the FPGA. For this reason, a multiplexing was required in the firmware in order make it possible to communicate with both i2c slaves independently. 

%-------------------------------------------------------------------
\subsection{DIO5 and TLU}
%-------------------------------------------------------------------

DIO5 i2c master ported out of the box from d19c- or OT-µDTC-firmware. The behaviour is quite simple, there is an enable and a config register which makes it possible to configure each of the five TTL channels of the FMC card. All channels can function as digital input or output, 50 Ohm input impedance termination can be enabled and threshold values can be applied. These functionalities makes it possible for us to use this card in a wide variety of applications.

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.7\textwidth}{tex/figures/DIO5_threshold.jpg}{tex/figures/DIO5_threshold.jpg}
      \caption[Caption for TOC]{Threshold of DIO5 in function of input voltage.\label{fig:test}}
}
\end{figure}

The Triggering and Logical Unit (TLU) is used dependently with the fast command processor block in OT/IT-µDTC-firmwares where few modifications were made. It is mainly responsible for delaying and measuring the phases of the triggers. In TTC-µDTC-firmware it has not been tested and used yet. We will definitely need to modify this block to meet the needs of this firmware before using with the TLU.  

%-------------------------------------------------------------------
\subsection{8SFP}
%-------------------------------------------------------------------

The i2c master in the system core was ported to the user core to program the SFP adapter on the 8SFP FMC card and pull high the TX$\_$disable through the IO expander. A common design consideration is that the FMC card is not energized by default and the laser diodes are turned off. Simulations and tests were performed carefully to verify the expected behaviour of the optical link in loopback mode. Simple python scripts are available to program the i2c master and enable the transceivers, which are also included in the TTC-FC7-Control Ph2\_ACF.

%-------------------------------------------------------------------
\section{Clock and measurement}
%-------------------------------------------------------------------

MMCM in the user core can be driven by many input sources. Default clock source is the fabric clock, generated by a local quartz crystal oscillator on FC7, where frequency is equivalent to the LHC 40.08 MHz clock. It is possible to switch the default clock source to an external clock signal received from DIO5 channel 5. External clock can be enabled either by BUFGMUX in clock generator block or by switching xpoint output on FC7. Output clocks generated from one of these clock sources with the MMCM are 40MHz, 80MHz, 160MHz, 200MHz, 320MHz and 400MHz respectively. There is an additional PLL driven from the same clock source as the MMCM, it is responsible for generating a 400MHz clock for the L1A phase measurement block. View the clocking diagram in the TCDS site or in the documents for further details.

There is another 125MHz local quartz crystal oscillator on FC7 which is the ipbus clock source in the system core. This is also used for generating 200MHz and 400MHz reference clocks for the IODELAYs in user core with an MMCM. In OT-µDTC-firmware it was used for driving the IODELAY in the TTC decoder block, but it has not been used, even in the production system.

One more quartz crystal oscillator is available on the FC7 card, but it is not particularly in use. It is a clock generator programmable up to 1GHz with an i2c master.

Finally, a 1Hz clock is also generated for a GTX earlier, but it is not used at this point, because in case of 8SFP FMC the GTX differential TX and RX pin pairs are routed to the select IOs of the FPGA directly and driven by a laser diode. It simplifies the problem to a great extent, since we do not need to drive the GTX of the FPGA with an IP generated by a GT Wizard.

\textbf{Monitoring}

Clock rate tool was implemented for frequency monitoring just for the sake of debugging the clocks inside the firmware. The following clocks are measured and can be read out via ipbus:

\begin{itemize}
\item \texttt{ipb\_clk} 31.25MHz
\item \texttt{fabric\_clk} 40.08MHz
\item \texttt{clk\_40MHz} clock generator output 40MHz
\item \texttt{clk\_80MHz} clock generator output 80MHz for TTC
\item \texttt{clk\_160MHz} clock generator output 160MHz for TTC
\item \texttt{osc125\_a\_mgtrefclk\_i} 125MHz onboard quartz crystal oscillator
\item \texttt{clk\_200MHz} clock generator output 200MHz for L1A phase measurement and TTS decoder
\item \texttt{clk\_400MHz} clock generator output 400MHz for L1A phase measurement
\item \texttt{ref\_clk\_200MHz} reference clock generator output 200MHz for TTC\_if
\end{itemize}

%-------------------------------------------------------------------
\section{TTC L1A trigger}
%-------------------------------------------------------------------

Level 1 trigger generation is one of the most important functionality the TTC provides. As soon as a trigger pulse is fired in a synchronous readout chain, it is expected that the event data will be read out at one point in time without any error or loss. Triggers can be generated in several ways, then filtered out or vetoed by specific trigger rules and their phases can be measured.

%-------------------------------------------------------------------
\subsection{Trigger generation}
%-------------------------------------------------------------------

There are four options for generating L1 triggers, each of which can be sent using pyTTC python scripts or the TTC-FC7-Control Ph2\_ACF acquisition framework software:

\begin{itemize}
\item \texttt{ipbus trigger} \\ Ipbus communication goes at 31.25MHz in frames. It is possible to send one or more triggers
at random moments. Randomness is the downside of this method.
\item \texttt{external trigger} \\ Sequences of externally generated trigger pulses of a source meter
or a scintillator can be tested with this method. 
\item \texttt{asynchronous trigger generator} \\ The generation of the asynchronous trigger can be derived from the handover of a 400MHz
clock and its relation to the LHC clock. A coarse and a fine delay value are available to generate high-precision triggers, which is
especially suitable for testing the L1A phase measurement. 
\item \texttt{clocked trigger generator} \\ Generating a clocked trigger is very useful in testing because the triggers are in phase
with the LHC clock and the phase of a given trigger is the quotient of the LHC clock and the trigger interval, where the trigger interval
is the dependent variable, this can be specified. 
\end{itemize}

%-------------------------------------------------------------------
\subsection{Trigger rules}
%-------------------------------------------------------------------

Three possible settings for trigger rules are possible:

\begin{itemize}
\item \texttt{no trigger rule} \\ There is no trigger rule defined by default.
\item \texttt{standard trigger rule} \\ There can be only a single trigger in every second BX.
\item \texttt{2S Phase-2 trigger rule} \\ Specified and testing required.
\item \texttt{PS Phase-2 trigger rule} \\ Not yet specified.
\end{itemize}

%-------------------------------------------------------------------
\subsection{L1A phase measurement}
%-------------------------------------------------------------------

L1A phase measurement block is responsible for the bookkeping of consecutive L1A trigger's phase. It is a counter that counts from 0 to 127 during the period of the 40.08MHz LHC clock with a clocked frequency of 5.12GHz derived from the LHC clock. When the leading edge of a trigger arrives, the value of the counter is copied into a 7-bit register and kept until the next trigger's leading edge. Instabilities has not taken into account yet, but the resolution of the measurement may be considered 0.2ns. For more information, view this presentation in the /doc folder \cite{bib:L1Aphprez}.

%-------------------------------------------------------------------
\section{TTC BGo}
%-------------------------------------------------------------------

Another goal of this bi-phase mark encoded TTC signal which drives the SFP input of AMC13 was to send out BGo commands in channel B in addition to sending triggers on channel A. Channel B processor block is responsible for sending out each BGo command, while BGo scheduler block schedules each BGo command and able to send out specific BGo sequences as well.

For sake of understanding, a simplified block diagram of the test setup can be seen on Figure 2.3 below. TTC-FC7 generates a common system clock to AMC13, which is routed to the TTC output connected to an optical fiber on 8 SFP FMC card. AMC13 distributes clock, trigger and BGos through the backplane of the µTCA crate to IT- or OT-FC7s through the TTC link. On the optical input of the 8 SFP FMC card, the actual TTS state is sent by AMC13 to the TTC-FC7, which can be read from ipbus registers after the TTS decoder decodes it. One more thing to note is, that IT- and OT-FC7s send their event, bunch and orbit numbers to AMC13 after every received L1A trigger. Comparison of event, bunch and orbit numbers happens in AMC13 and IT- or OT-FC7s as well. AMC13 changes the TTS state whenever a mismatch is generated for any counter after every L1A trigger event.

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.75\textwidth}{tex/figures/setup.png}{tex/figures/TTS_states-setup_3.png}
      \caption[Caption for TOC]{Simplified block diagram of the setup.\label{fig:test}}
}
\end{figure}

%-------------------------------------------------------------------
\subsection{Channel B processor and BGo scheduler}
%-------------------------------------------------------------------

As described above channel B processor block is responsible for sending BGo commands to IT- and OT-FC7s in the µTCA crate through the backplane. List of possible BGo commands and sequences can be seen in Table 2.1 below with associated broadcast commands.

\begin{table}[h]
\footnotesize
  \begin{center}
    \caption{implemented TTC/BGo commands and sequences}
    \label{tab:mytab}
    \begin{tabular}{llcl} \hline
       Command                  & Broadcast $<$7:0$>$     & Recognized by AMC13$?$ & Meaning                       \\ \hline
       Event-count Reset (EC0)  & xxxxxx1x                & Yes                    & Reset EvN                     \\
       Bunch-count Reset (BC0)  & xxxxxxx1                & Yes                    & Reset BcN                     \\
       Orbit-count Reset (OC0)  & 001x1xxx                & Yes                    & Reset OrN                     \\
       Resync                   & 010x1xxx                & Yes                    & Reset DAQ path                \\ \hline \hline
       Sequence                 & BGo code $<$7:0$>$      & Recognized by AMC13$?$ & Meaning                       \\ \hline
       Resync                   & x"14"                   & Yes                    & Resync + 64 Orb + EC0 + 1 Orb \\ \hline
    \end{tabular}
  \end{center}
\end{table}

%-------------------------------------------------------------------
\subsection{debugging}
%-------------------------------------------------------------------

TTC-µDTC firmware further includes two additional blocks, the TTC interface from the AMC13 firmware and the TTC decoder from the d19c firmware. The former is suitable for sending TTC commands and the latter for decoding them. These were used primarily for debugging purposes and it is possible to run simulations with them when one of them is enabled in the simulation package file. These are not required under normal operating conditions.

\clearpage

%-------------------------------------------------------------------
\section{TTS logic}
%-------------------------------------------------------------------

Each FC7 in the µTCA crate has a TTS FSM and except TTC-FC7 has their own TTS states, which is periodically sent back to AMC13. The main TTS states of the AMC13's state machine, without the auxiliary states, are shown in the following Table 2.2 below. As discussed earlier AMC13 sends out its actual TTS state to TTC-FC7 as well, on an optical fiber. Incoming TTS signal in TTC-FC7 is routed to an IDDR and an 8b/10b decoder decodes the stream, so that the actual TTS state can be checked via ipbus. As a result, if any FC7 flips to a new TTS state, it will also be followed by the AMC13's TTS FSM, which will also be acknowledged by the TTC-FC7.

\begin{table}[h]
  \begin{center}
    \caption{main TTS states of the AMC13's state machine (FSM)}
    \begin{tabular}{llll} \hline
       OT/IT TTS state                 & Meaning          & AMC13 TTS state & Meaning \\ \hline
       x"0" $|$ x"f"                   & disconnected     & SYN             & Synchronization lost \\
       x"1"                            & Overflow warning & OFW             & Overflow warning \\
       x"2"                            & Out of Sync      & SYN             & Synchronization lost \\
       x"4"                            & Busy             & BUSY            & BUSY \\
       x"c"                            & error            & SYN             & Synchronization lost \\
       x"8" $|$ x"9" $|$ x"a" $|$ x"b" & ready            & READY           & READY \\ \hline
    \end{tabular}
    \label{tab:mytab}
  \end{center}
\end{table}

Actual TTS state in the test setup can change in two ways. IT- and OT-FC7 generates a data frame for each incoming L1A trigger, in which it sends the particular counter numbers and TTS state to AMC13 from the amc13 decoder firmware block. AMC13 can change its TTS state based on IT- or OT-FC7's TTS state, if it is not in a synchronization error state (SYN). IT- or OT-FC7 counter numbers for each incoming frame is compared by AMC13 with its own counter numbers. In case of any counter mismatch, the system goes out-of-synch. Based on the test results, it is only possible to return from this state by sending a general reset or two resynch commands to AMC13 if IT- or OT-FC7 has previously changed its TTS state. In case of IT- and OT-FC7, TTS state can be specified manually by ipbus register or by the data processing block.

%-------------------------------------------------------------------
\subsection{TTS decoder and debugger}
%-------------------------------------------------------------------

Apart from TTS decoder TTC-µDTC firmware further includes one additional block, the TTS interface from the AMC13 firmware. It is suitable for sending TTS signal through ODDR and 8b/10b encoder. This is used primarily for debugging purposes and it is possible to run simulation with it when enabled in the simulation package file. This is not required under normal operating conditions, but it was very handy in the development process.

Finally, under the development process, it has become obvious that the signal delay caused by the length of the optical fiber can cause problem in the TTS signal decoding. This problem has been solved by a firmware process automatically locking to the incoming TTS signal with the help of IDELAY instance, which ensures that the TTS signal is decoded correctly. 

%-------------------------------------------------------------------
\subsection{TTS state machine}
%-------------------------------------------------------------------

There are three TTS state machines in the test setup. The 1st and simplest is in TTC firmware, which can only send BGo commands or sequences when specific state transitions occur. The 2nd is in AMC13, it is very complicated and its main features have already been described. The 3rd TTS FSM is ported in the IT and OT firmware. These are actually the same, but there may be minor differences between them due to design concepts. This TTS state machine was originally designed to handle out-of-synch (OOS) state, but it was removed at the discretion of management. In the following, the former TTS state machines will be described one by one.

\clearpage

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=1.4\textwidth, angle = 90}{tex/figures/TTS_FSMs_detailed..png}{tex/figures/TTS_FSMs_detailed..png}
      \caption[Caption for TOC]{TTS state machines in TTC-µDTC and IT- or OT-µDTC.\label{fig:test}}
}
\end{figure}

\clearpage

%-------------------------------------------------------------------
\subsubsection{TTS state machine in IT- and OT-µDTC}
%-------------------------------------------------------------------

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.75\textwidth}{tex/figures/ITOT_TTS_FSM_detailed.png}{tex/figures/ITOT_TTS_FSM_detailed.png}
      \caption[Caption for TOC]{TTS state machine in TTC-µDTC.\label{fig:test}}
}
\end{figure}

\begin{footnotesize}

Description of TTS FSM in OT-µDTC

\textbf{Start-up} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
IDLE → READY      & - default state is IDLE \\
                  & - configuration of fast command block is necessary to go to READY state \\
                  & - reset brings back the system to default state
\end{tabular}

\begin{left} \textbf{Busy mechanism/trigger throttling} \end{left}  \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
READY → BUSY\_1   & - if any buffer is full, we go back to the BUSY\_1 state and suspend the sending of triggers again by vetoing triggers in TTC-FC7 \\
BUSY\_1 → READY   & - when all buffers are empty, READY state is restored and triggers can be accepted again
\end{tabular}

\begin{left} \textbf{Synchronization lost \color{red}(not fully implemented, as agreed)} \end{left} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
READY → OOS \&    & - if there is EvN, BcN and/or OrN mismatch, OOS state transition takes place \\
BUSY\_1 → OOS     & \\
                  & \\
OOS → BUSY\_2     & - after OOS state takes effect, resync sequence sent by the TTC board received
\end{tabular}

Why to send resync from BUSY\_1 or READY states ? \\
Each time one FC7 drops out-of-sync in a uTCA crate, AMC13 goes to SYN state and the rest of FC7s in the crate could
be in different TTS state other than OOS.

\begin{tabular}{p{3cm} p{12.5cm}}
BUSY\_2 → BUSY\_3 & - when all triggers sent out to the FEs, BUSY\_3 state takes place \\
                  & \\
BUSY\_3 → BUSY\_1 & - when data readout is finished BUSY\_1 state takes place
\end{tabular}

\underline{Notes:} \linebreak
If the OOS TTS state is not required, triggers appearing at the output of the TTC decoder must be connected to the input of the AMC13 block so that AMC13 receives the acknowledge signal for each trigger from OT-µDTC, in which case no mismatch is created in AMC13.

Legend

\textbf{TTC resync}                                      : TTC resync command decoded by TTC decoder \\
\textbf{mismatch}                                        : generated in AMC13 block when counter values compared and not matched for EvN BcN OrN \\
\textbf{any\_buffer\_full}                               : controlled by be\_proc/ipbus when backpressure\_enable is enabled/disabled in fast command \\
\textbf{all\_buffer\_empty}                              : controlled by be\_proc/ipbus when backpressure\_enable is enabled/disabled in fast command \\
\textbf{stat\_be\_proc\_i.evnt\_cnt\_buf\_empty}         : event counter buffer empty in be\_proc, default value is '1' \\
\textbf{stat\_readout\_i.words\_to\_read\_cnt = x"0000"} : words to read count in readout, default value is x"0000"

\clearpage

\textbf{be\_proc\_any\_buffer\_full} : one of the buffers almost full in data processing block

backpressure $<=$ (evnt\_cnt\_buf\_prog\_full or TDC\_cnt\_buf\_prog\_full or tlu\_fifo\_prog\_full or buffer\_backpressure) when (tlu\_data\_i.tlu\_enabled = '1' and tlu\_data\_i.handshake\_mode = "10") else (evnt\_cnt\_buf\_prog\_full or TDC\_cnt\_buf\_prog\_full or buffer\_backpressure);

\textbf{be\_proc\_all\_buffer\_empty} : all buffers almost empty in data processing block

data\_payload\_buf\_empty $<=$ (evnt\_cnt\_buf\_empty or TDC\_cnt\_buf\_empty or tlu\_fifo\_empty) when (tlu\_data\_i.tlu\_enabled = '1' and tlu\_data\_i.handshake\_mode = "10") else (evnt\_cnt\_buf\_empty or TDC\_cnt\_buf\_empty);

\textcolor{red}{Note: Buffer full and empty flags or tresholds must be specified in Vivado FIFO IPs!}

\textbf{stat\_be\_proc\_i.evnt\_cnt\_buf\_empty} : event counter buffer empty in be\_proc, default value is '1'

Here, we could even check if the buffers of the FEs have been cleared...

\textbf{stat\_readout\_i.words\_to\_read\_cnt} : words to read count in readout, default value is x"0000"

This is a specific state of the state machine in the readout block, which indicates how many words are left to read. It is just an option to select this condition, other conditions can be specified as well!

\end{footnotesize}

%-------------------------------------------------------------------
\subsubsection{TTS state machine in TTC-µDTC}
%-------------------------------------------------------------------

\begin{figure}[!htb]
\centering
\vbox{
      \combinedfigure{width=0.55\textwidth}{tex/figures/TTC_TTS_FSM.png}{tex/figures/TTC_TTS_FSM.png}
      \caption[Caption for TOC]{Simplified TTS state machine in TTC-µDTC.\label{fig:test}}
}
\end{figure}

\begin{footnotesize}

Description of TTS FSM in TTC-µDTC

\textbf{Start-up} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
IDLE → BUSY       & - default state is IDLE \\
                  & - reset brings the system to BUSY state
\end{tabular}

\begin{left} \textbf{Busy mechanism/trigger throttling} \end{left} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
BUSY → READY      & - if READY (x”8”) TTS state is decoded, READY state transition takes place \\
                  & - triggers allowed only in READY TTS state when trigger veto is 0 \\
READY → BUSY      & - if BUSY (x”4”) TTS state is decoded, BUSY state transition takes place
\end{tabular}

\begin{left} \textbf{Synchronization lost} \end{left} \linebreak
\begin{tabular}{p{3cm} p{12.5cm}}
BUSY → OOS\_1 \&  & - when OOS (x”2”) TTS state is decoded, TTC sends a resync sequence \\
READY → OOS\_1    &
\end{tabular}

Why OOS\_1 → READY TTS state transition is necessary ? \\
After OT-µDTC sends OOS, AMC13 goes to SYN state. It is possible in certain conditions that after the first resync sequence sent by TTC-µDTC to OT-µDTC, TTS state changes in AMC13 directly from SYN to RDY and BUSY (x”4”) state transition does not happen.

\begin{tabular}{p{3cm} p{12.5cm}}
OOS\_1 → OOS\_2   & - If after the resync sequence the TTS state is BUSY (x”4”), OOS\_2 TTS state transition takes place and TTC sends another resync sequence \\
OOS\_2 → READY    & - as READY TTS state appears after the second resync sequence, READY state transition takes place
\end{tabular}

\end{footnotesize}

\clearpage

%-------------------------------------------------------------------
\subsection{TTS state emulator}
%-------------------------------------------------------------------

Apart from TTS state machine TTC-µDTC firmware further includes one additional block, the TTS state emulator partly from the d19c firmware. A fixed pattern is sent in the simulation, from which the numeric values of the counters followed by the TTS state are read from a frame belonging to an L1A trigger event. Furthermore, check that the values of the read counters are the same as the values of the counters in the firmware at that time. If there is no match, a mismatch occurs and the TTS state is switched using a simple state machine. This is used primarily for debugging purposes, developing TTS state machines and it is possible to run simulation with it when enabled in the simulation package file. This is not required under normal operating conditions, but it was very handy in the development process.

%-------------------------------------------------------------------
\section{Transition Logger and counters}
%-------------------------------------------------------------------

TTC-µDTC firmware contains 4 counters that are essential for the orbit structure. These are Event Counter, Event Reject Counter, Bunch Counter and Orbit Counter respectively. The Event reject counter is needed because we also want to know that a loose trigger has been dropped. The data for each event is written to a 128k FIFO in the form of 32-bit queue vectors, which are read by the Ph2-ACF readBlock function call \cite{bib:ipbus-software}. During an LHC clock period, a 160 MHz clock collects the current values of the counters and TTS state from the FSM at each BX time on rising edge. Event data in the queue vector contains the following entries:

\textbf{Entry} : Msg ID, time stamp, L1A phase / BC0 / TTS state / BGo. 

Below is an excerpt from the log of the later test:

\textcolor{gray}{
Number of state transition log entries  36      \\
000000 :  [TTS], BCN= 1697 , TTS= RDY(8)        \\
000001 :  [BC0], ORB= 69377 , BC0\_en= 0        \\
000002 :  [TTS], BCN= 3177 , TTS= BSY(4)        \\
000003 :  [BC0], ORB= 69411 , BC0\_en= 0        \\
000004 :  [TTS], BCN= 3033 , TTS= RDY(8)        \\
000005 :  [BC0], ORB= 69470 , BC0\_en= 0        
}

Further detailed discriptions can be found in presentations in the /doc folder \cite{bib:Tlogprez}.

\clearpage
