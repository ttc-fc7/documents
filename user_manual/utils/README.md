# Included as a subproject for tdr documents.

Help on tdr is available at the Twiki, https://twiki.cern.ch/twiki/bin/view/CMS/Internal/TdrProcessing, and from invoking help 'tdr -h'

# Guide to the general folder

This folder contains items of general utility in creating and manipulating CMS documents.

## files for creating documents

|||
|---|---|
|cms-tdr.cls | LaTeX class file for all CMS TeX documents|
|BigDraft.pdf | background watermark image for draft version|
|skeleton_*.tex | required beginning and ending files included with document body|

## LaTeX style files

|||
|---|---|
|pdfdraftcopy.sty | creates watermark for PDF draft versions|
